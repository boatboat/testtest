//
//  DataServices.swift
//  projectTest
//
//  Created by Chavin  charoenvitvorakul on 2/5/2561 BE.
//  Copyright © 2561 bbbb. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DataServices {
    
    
    static let instance = DataServices()
    
    
    func getTempFromString(input : String, completion : @escaping CompletionHandler) {
        
        var parameters = ["q" : input,
                          "appid" : "00ca44962f8f1d7625dc2229f1c5baf0",
                          ]
        
//        print(BASE_URL+"&weather?q=\(input)")
        
//        print(BASE_URL)
        
//        Alamofire.request(BASE_URL+"&weather?q=\(input)").responseJSON { (response) in
//            print(response)
//        }
        
//        Alamofire.request(BASE_URL+"&weather?q=\(input)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
//            print(response)
//
//
//        }
        
        
        Alamofire.request(URL_WEATHER, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: HEADER).responseJSON { (response) in
            
            print(response.request?.url)
            if response.result.error == nil {

                guard let data = response.data else { return }
                let json = JSON(data)
//print(json.rawString())
                completion(true,["msg":json.rawString()])

            }else{
                completion(false,["msg":"ไม่สามารถเชื่อมต่อกับเซิฟเวอร์ได้"])
            }

        }
    }
}

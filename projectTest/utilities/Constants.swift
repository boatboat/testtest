//
//  Constants.swift
//  projectTest
//
//  Created by Chavin  charoenvitvorakul on 2/5/2561 BE.
//  Copyright © 2561 bbbb. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool,_ msg: [String: String?]) -> ()

//let BASE_URL = "http://api.openweathermap.org/data/2.5/"

let BASE_URL = "http://api.openweathermap.org/data/2.5/"

let URL_WEATHER = BASE_URL+"weather"


var HEADER = [ "Content-type": "application/json; charset=utf-8"]

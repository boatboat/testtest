//
//  ViewController.swift
//  projectTest
//
//  Created by Chavin  charoenvitvorakul on 2/5/2561 BE.
//  Copyright © 2561 bbbb. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {
    
    var tempe = temp()
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var lblC: UILabel!
    @IBOutlet weak var fieldText: UITextField!
    @IBOutlet weak var lblF: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.stopAnimating()
        
        fieldText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func btnPressed(_ sender: Any) {
        
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        indicator.startAnimating()
        
        DataServices.instance.getTempFromString(input: fieldText.text!) { (success, msg) in
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            if success {
                var json = JSON.init(parseJSON: msg["msg"]!!)
                self.tempe = temp.init(json: json)
                
                if self.tempe.tempK != nil {
                
                self.lblC.text = "\(self.tempe.tempC!)"
                self.lblF.text = "\(self.tempe.tempF!)"
                    
                }
                //change label
            }
        }
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

